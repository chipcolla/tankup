import json

import aiohttp
import asyncpg
from aiohttp import web

from settings import SQL, DB_SETTINGS


# Получение координат всех заправщиков.
async def all_drivers_coord(id_list: list) -> str:
    try:
        conn = await asyncpg.connect(**DB_SETTINGS)
        driver = await conn.fetch(SQL.get('manager').get('ws')[0].format(str([d.get('id') for d in id_list])))
        await conn.close()
        return json.dumps(driver)
    except Exception as e:
        return json.dumps(['Ошибка выполнения запроса: all_drivers_coord'])


class WebSocket(web.View):
    """
    Веб-сокеты.
    """
    async def get(self):
        ws = web.WebSocketResponse()
        await ws.prepare(self.request)
        self.request.app['sockets'].append(ws)

        try:
            async for msg in ws:
                if msg.type == aiohttp.WSMsgType.TEXT:
                    if msg.data == 'close':
                        await ws.close()
                    else:
                        for _ws in self.request.app['sockets']:
                            d = await all_drivers_coord(json.loads(msg.data))
                            await _ws.send_str(d)
                            # await _ws.send_str(msg.data)
                        # return await ws.send_str(msg.data)
                elif msg.type == aiohttp.WSMsgType.ERROR:
                    print('ws connection closed with exception %s'.format(ws.exception()))
            return ws
        finally:
            self.request.app['sockets'].remove(ws)
