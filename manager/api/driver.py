import json

from aiohttp import web
from aiohttp.web_response import json_response

from db.db import fetch
from decorators import check_body_exist
from settings import SQL
from utils.api import ApiReturnAnswer


class GetAllDriversAPI(web.View):
    """
    Апи для получения всех заправщиков.
    """
    async def get(self):
        try:
            all_driver = await fetch(self.request.app['pool'], SQL.get('manager').get('driver')[0])

            all_order = await fetch(self.request.app['pool'], SQL.get('manager').get('driver')[1].format(
                str([d.get('id') for d in all_driver])))

            for driver in all_driver:
                orders_list = list()
                for order in all_order:
                    if driver.get('id') == order.get('driver'):
                        order['updated_at'] = order.get('updated_at').strftime("%Y-%m-%d %H:%M:%S")
                        orders_list.append(order)
                driver['orders'] = orders_list

            if not all_driver:
                return ApiReturnAnswer.bad('Заправщиков не найдено.')

            return json_response(all_driver)
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка в запросе, для получения всех заправщиков.')


class CreateDriversAPI(web.View):
    """
    Апи для создания заправщика.
    """

    @check_body_exist
    async def post(self):
        try:
            post_data = await self.request.json()
            driver_name = post_data.get('name')
            driver = await fetch(self.request.app['pool'], SQL.get('manager').get('driver')[2].format(
                driver_name,
                post_data.get('login'),
                True
            ))
            return json_response(driver.pop())
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка в запросе, для добавления заправщика.')

