from aiohttp import web
from aiohttp.web_response import json_response

from db.db import fetch
from decorators import check_body_exist
from settings import custom_pbkdf2, SQL
from utils.api import ApiReturnAnswer
from utils.jwt_helper import build_token


class ManagerAuthAPI(web.View):
    """
    Апи для проверки логина и пароля пользователя менеджера.
    """
    TOKEN_VERIFY = False

    @check_body_exist
    async def post(self):
        try:
            post_data = await self.request.json()
            user = await fetch(self.request.app['pool'], SQL.get('manager').get('manager_auth')[0].format(
                post_data.get('login')))
            if not user:
                return ApiReturnAnswer.bad('Менеджера: {} не найдено.'.format(post_data.get('login')))
            user_password = user[0].get('password')
            # if user:
            if user and custom_pbkdf2.verify(post_data.get('password'), user_password):
                user = user.pop()
                user['token'] = build_token(user)
                user.pop('password', None)
                return json_response(user)
            else:
                return ApiReturnAnswer.bad('Менеджер: {} не прошел авторизацию.'.format(post_data.get('login')))
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка при проверке менеджера.')
