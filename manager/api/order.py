import json

import aiohttp_cors
from aiohttp import web
from aiohttp.web_response import json_response
from aiohttp_cors import CorsViewMixin

from db.db import fetch
from decorators import check_body_exist
from models.tankup_model import ORDER_STATUS_CHOICES
from settings import SMS_MESSAGES_TEMPLATE, QUICK_FUEL_HELPER, SQL
from utils.api import ApiReturnAnswer
from utils.message import combined_message


class FindAPI(web.View):
    """
    Апи для получения всех заказов.
    """

    async def get(self):
        try:
            orders = await fetch(self.request.app['pool'], SQL.get('manager').get('order')[0])
            if not orders:
                return ApiReturnAnswer.bad('Заказы не найдены.')
            for order in orders:
                order['updated_at'] = order.get('updated_at').strftime("%Y-%m-%d %H:%M:%S")

            return json_response(orders)
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка в запросе, для получения всех заказов.')


class AssignOrderToDriverAPI(web.View):
    """
    Апи для привязки заказа к заправщику.
    """

    @check_body_exist
    async def post(self):
        try:
            post_data = await self.request.json()

            order = await fetch(self.request.app['pool'], SQL.get('manager').get('order')[1].format(
                post_data.get('driver_id', 0), post_data.get('order_id', 0)))
            if not order:
                return ApiReturnAnswer.bad(
                    'Не получилось назначить заказ: {}, заправщику: {}.'.format(
                        post_data.get('order_id'), post_data.get('driver_id')))

            order = order.pop()

            # Получим заправщика.
            driver = await fetch(self.request.app['pool'], SQL.get('manager').get('order')[2].format(
                post_data.get('driver_id', 0)
            ))
            if not driver:
                return ApiReturnAnswer.bad('Не получилось получить водителя: {}.'.format(post_data.get('driver_id')))

            # Для клиента
            await combined_message(order.get('mp_id'), order.get('phone_number'),
                                   message=SMS_MESSAGES_TEMPLATE.get('order-assign-user'))

            oad_text = SMS_MESSAGES_TEMPLATE.get('order-assign-driver').format(
                order.get('name'),
                order.get('car_model'),
                order.get('car_no'),
                QUICK_FUEL_HELPER[order.get('fuel_id')],
                order.get('liters')
            )

            # Для заправщика
            await combined_message(driver[0].get('mp_id'), driver[0].get('login'), message=oad_text)

            return json_response(order)
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка в запросе привязки заказов для заправщика.')


class SetOrderStatusAPI(web.View):
    """
    Апи для установки статуса заказу.
    """
    @check_body_exist
    async def post(self):
        try:
            post_data = await self.request.json()

            order_status_id = post_data.get('order_status_id')

            order = await fetch(self.request.app['pool'], SQL.get('manager').get('order')[3].format(
                order_status_id, post_data.get('order_id')))
            if not order:
                return ApiReturnAnswer.bad(
                    'Не получилось установить заказу id={}, статус id={}. Заказ не найден!'.format(
                        post_data.get('order_id'), post_data.get('order_status_id')))

            if int(order_status_id) == dict(ORDER_STATUS_CHOICES).get('completed'):

                await combined_message(order[0].get('mp_id'), order[0].get('phone_number'),
                                       message=SMS_MESSAGES_TEMPLATE.get('order-complete-user'))
            return json_response(order)
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка в запросе установки статуса для заказа.')

