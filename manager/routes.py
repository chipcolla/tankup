import collections

from manager.api.driver import GetAllDriversAPI, CreateDriversAPI
from manager.api.manager_auth import ManagerAuthAPI
from manager.api.order import FindAPI, AssignOrderToDriverAPI, SetOrderStatusAPI
from manager.ws import WebSocket
from refueller.api.order import SetCoordForDriverAPI, GetCoordForDriverAPI
from user.api.fuel import FuelAPI
from user.api.geo import GetAddressFromCoordinatesAPI, GetCoordinatesFromAddressAPI

dispatcher = collections.namedtuple('Dispatcher', ['method', 'path', 'handler', 'name'])


manager_routes = [
    # API
    dispatcher('POST', '/api/v2/manager/auth/', ManagerAuthAPI, 'api_auth_manager'),
    dispatcher('GET', '/api/v2/manager/fuel/', FuelAPI, 'api_manager_fuel'),
    dispatcher('GET', '/api/v2/manager/find/', FindAPI, 'api_manager_find'),
    dispatcher('GET', '/api/v2/manager/driver/all/', GetAllDriversAPI, 'api_driver_all'),
    dispatcher('POST', '/api/v2/manager/driver/create/', CreateDriversAPI, 'api_driver_create'),
    dispatcher('POST', '/api/v2/manager/order/assign/', AssignOrderToDriverAPI, 'api_order_assign'),
    dispatcher('POST', '/api/v2/manager/order/set/status/', SetOrderStatusAPI, 'api_order_set_status'),
    dispatcher('POST', '/api/v2/manager/coord/address/', GetAddressFromCoordinatesAPI, 'api_manager_address_coord'),
    dispatcher('POST', '/api/v2/manager/address/coord/', GetCoordinatesFromAddressAPI, 'api_manager_coord_address'),
    dispatcher('POST', '/api/v2/manager/coord/set/', SetCoordForDriverAPI, 'api_coord_set_manager'),
    dispatcher('GET', '/api/v2/manager/coord/get/', GetCoordForDriverAPI, 'api_coord_get_manager'),
    dispatcher('GET', '/manager/ws/', WebSocket, 'web_socket'),
]
