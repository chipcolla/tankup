import multiprocessing
from concurrent.futures.thread import ThreadPoolExecutor

from passlib.handlers.pbkdf2 import pbkdf2_sha256
from pyfcm import FCMNotification
from ruamel.yaml import YAML


DB_SETTINGS = {
    'host': '127.0.0.1',
    'user': '',
    'password': '',
    'database': '',
    'port': 5432,
    'min_size': 0,
    'max_size': 3
}

GLOBAL_SECRET = {
    'manager': '',
    'refueller': ''
}

SMS_MESSAGES_TEMPLATE = {
    'check-sms': 'TankUp!%20Ваш%20проверочный%20код%0A{}',
    'order-assign-user': 'TankUp!%20Ваш%20заказ%20назначен%20водителю!',
    'order-complete-user': 'TankUp!%20Ваша%20машина%20готова%20к%20приключениям!',
    'order-assign-driver': 'TankUp!%20Назначен%20заказ.%0AКлиент%20{},%20марка%20{},%20номер%20{}.%0AБензин%20{},%20{}%20литров.',
    'order-complete-driver': 'TankUp!%20Заказ%20завершен!',
}

QUICK_FUEL_HELPER = {
    1: 'АИ-92',
    2: 'АИ-95',
    3: 'ДТ'
}

custom_pbkdf2 = pbkdf2_sha256.using(rounds=120000)

pool = ThreadPoolExecutor(max_workers=multiprocessing.cpu_count())

push_service = FCMNotification(api_key="")


YANDEX_GEO_TOKEN = ('', '',)

with open('db/sql_query.yaml', 'r') as content_file:
    yaml = YAML(typ='safe')
    SQL = yaml.load(content_file.read())

