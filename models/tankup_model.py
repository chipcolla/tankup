"""
Модель для миграции в БД
"""
import datetime

from passlib.handlers.pbkdf2 import pbkdf2_sha256
from peewee import *
from playhouse.migrate import PostgresqlMigrator, migrate

pg_db = PostgresqlDatabase('tankup', user='tankup_user', password='puknat', host='127.0.0.1', port=5432)

PAYMENT_TYPE_CHOICES = (
    # Pay системы
    ('ApplePay', 0),
    ('GooglePay', 0),
    # Картой
    ('Card', 1),
)

USER_TYPE_CHOICES = (
    # Менеджер
    ('manager', 1),
    # Заправщик
    ('refueller', 2),
)

ORDER_STATUS_CHOICES = (
    # Оплачен
    ('paid', 1),
    # Назначен
    ('assigned', 2),
    # Отменен
    ('canceled', 3),
    # Завершен
    ('completed', 4),
)


class BaseModel(Model):
    class Meta:
        database = pg_db


class FuelType(BaseModel):
    name = CharField(unique=True)
    price = DoubleField()
    updated_at = DateTimeField()


class CarModel(BaseModel):
    name = CharField()
    updated_at = DateTimeField()


class Account(BaseModel):
    # Реальное имя пользователя, если нужно.
    name = CharField(max_length=255, null=True)
    login = CharField(max_length=255, unique=True)
    password = CharField(max_length=255, null=True)
    email = CharField(max_length=255, null=True)
    user_type = IntegerField(choices=USER_TYPE_CHOICES)
    is_active = BooleanField(default=True)
    verified_at = DateTimeField(null=True)
    activation_code = CharField(max_length=255, null=True)
    # Для заправщика: широта и долгота
    lat = DoubleField(null=True)
    lon = DoubleField(null=True)
    mp_id = CharField(null=True, unique=True)


class Orders(BaseModel):
    name = CharField(default='')
    liters = IntegerField()
    fuel = ForeignKeyField(FuelType, null=False, backref='fuel_order')
    driver = ForeignKeyField(Account, null=True, backref='driver_orders')
    car_model = CharField()
    car_no = CharField()
    phone_number = CharField()
    lat = DoubleField()
    lon = DoubleField()
    order_status = IntegerField(choices=ORDER_STATUS_CHOICES, default=1)
    payment_type = IntegerField(choices=PAYMENT_TYPE_CHOICES, default=3, null=True)
    mp_id = CharField(null=True, unique=False)
    updated_at = DateTimeField()


class Cache(BaseModel):
    lat = DoubleField(null=True, index=True)
    lon = DoubleField(null=True, index=True)
    address = CharField(null=True, index=True)
    use_cache_count = IntegerField(default=0, null=True, index=True)
    updated_at = DateTimeField()


# pg_db.create_tables([Cache])
# pg_db.drop_tables([Account])
# pg_db.create_tables([Account])
# pg_db.close()

