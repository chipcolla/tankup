import datetime

from models.tankup_model import FuelType

# data_insert = datetime.datetime.now().strftime('%Y-%m-%d %H:%M:%S')

data_source = [
    {'name': 'АИ-92', 'price': 39.50, 'updated_at': datetime.datetime.now()},
    {'name': 'АИ-95', 'price': 42.25, 'updated_at': datetime.datetime.now()},
    {'name': 'ДТ', 'price': 44.80, 'updated_at': datetime.datetime.now()},
]

FuelType.insert_many(data_source).execute()
