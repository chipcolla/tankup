import functools

from aiohttp.web_response import json_response


def check_body_exist(func):
    @functools.wraps(func)
    async def wrapper(*args, **kwargs):
        try:
            if not args[0].request.body_exists:
                return json_response({'message': 'Пустое тело запроса к АПИ.'}, status=400)
            return await func(*args, **kwargs)
        finally:
            pass
    return wrapper
