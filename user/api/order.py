import asyncio
import datetime
import json

from aiohttp import web
from aiohttp.web_response import json_response

from db.db import fetch
from decorators import check_body_exist

from models.tankup_model import ORDER_STATUS_CHOICES
from settings import SQL
from utils.api import ApiReturnAnswer


class CreateOrderAPI(web.View):
    """
    Апи для создания заказа.
    """

    @check_body_exist
    async def post(self):
        try:
            post_data = await self.request.json()
            order_status = dict(ORDER_STATUS_CHOICES).get(post_data.get('orderStatus'), 1)

            # Получим id типа топлива.
            if post_data['fuel'].get('id'):
                fuel_id = post_data['fuel'].get('id')
            else:
                fuel_id = await fetch(self.request.app['pool'],  SQL.get('user').get('order')[0].format(
                    post_data['fuel'].get('name')))
                fuel_id = fuel_id.pop().get('id')

            # Проверка дублей. Почему - то приходят с мобил.
            check_order = await fetch(self.request.app['pool'], SQL.get('user').get('order')[3].format(
                post_data.get('carNo'),
                post_data.get('phoneNumber'),
                post_data['location'].get('lat'),
                post_data['location'].get('lon'),
                post_data.get('carModel'),
                post_data.get('name')
            ))

            if check_order:
                answer = check_order.pop()
            else:
                create_order = await fetch(self.request.app['pool'], SQL.get('user').get('order')[1].format(
                    post_data.get('name'),
                    post_data.get('liters'),
                    fuel_id,
                    post_data.get('carNo'),
                    post_data.get('carModel'),
                    post_data.get('phoneNumber'),
                    post_data['location'].get('lat'),
                    post_data['location'].get('lon'), order_status,
                    post_data.get('paymentType'),
                    post_data.get('user_id')
                ))
                answer = create_order.pop()

            await asyncio.sleep(1)
            create_order = await fetch(self.request.app['pool'], SQL.get('user').get('order')[4].format(
                post_data.get('carNo'),
                post_data.get('phoneNumber'),
                post_data['location'].get('lat'),
                post_data['location'].get('lon'),
                post_data.get('carModel'),
                post_data.get('name'),


                post_data.get('name'),
                post_data.get('liters'),
                fuel_id,
                post_data.get('carNo'),
                post_data.get('carModel'),
                post_data.get('phoneNumber'),
                post_data['location'].get('lat'),
                post_data['location'].get('lon'), order_status,
                post_data.get('paymentType'),
                post_data.get('user_id')
            ))
            answer = create_order.pop()

            expires = datetime.datetime.utcnow() + datetime.timedelta(days=1)
            response = web.Response(body=json.dumps(answer))
            response.set_cookie('order_id', answer.get('id'), expires=expires.strftime("%a, %d %b %Y %H:%M:%S GMT"))
            return response
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка при создании нового заказа.')


class GetActiveOrderAPI(web.View):
    """
    Апи для получения текущего (активного) заказа.
    """
    async def get(self):
        try:
            order_id = self.request.cookies.get('order_id', 0)

            active_order = await fetch(self.request.app['pool'], SQL.get('user').get('order')[2].format(order_id))
            if not active_order:
                return json_response(dict())
            else:
                active_order = active_order.pop()
                answer = {
                    'id': active_order.get('id'),
                    'liters': active_order.get('liters'),
                    'carNo': active_order.get('car_no'),
                    # Чем оплачено? ApplePay или картой!
                    'paymentType': active_order.get('payment_type'),
                    'orderStatus': active_order.get('order_status'),
                    'carModel': active_order.get('car_model'),
                    'name': active_order.get('o_name'),
                    'phoneNumber': active_order.get('phone_number')
                }

                driver_lat = active_order.get('driver_lat')
                driver_lon = active_order.get('driver_lon')

                if driver_lat and driver_lon:
                    answer['driver'] = {
                        'location': {
                            'lat': driver_lat,
                            'lon': driver_lon
                        }
                    }
                else:
                    answer['driver'] = {
                        'location': {}
                    }

                answer['fuel'] = {
                    'id': active_order.get('ft_id'),
                    'name': active_order.get('ft_name'),
                    'price': active_order.get('ft_price')
                }

                answer['location'] = {
                    'lat': active_order.get('lat'),
                    'lon': active_order.get('lon')
                }

                return json_response(answer)
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка в запросе, при получении текущего заказа.')

