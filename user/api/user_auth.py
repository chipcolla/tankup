from random import randint

from aiohttp import web
from aiohttp.web_response import json_response
from passlib.handlers.pbkdf2 import pbkdf2_sha256

from db.db import fetch
from utils.sms import sms_main

custom_pbkdf2 = pbkdf2_sha256.using(rounds=120000)


class ManagerAuthAPI(web.View):
    """
    Апи для проверки логина и пароля пользователя менеджера.
    """
    async def post(self):
        post_data = await self.request.json()
        try:
            user = await fetch(self.request.app['pool'],
                               "select * from account where login='{}' "
                               "and user_type={} and is_active=True".format(
                                   post_data.get('login'), 1))
            user_password = user[0].get('password')
            if user and custom_pbkdf2.verify(post_data.get('password'), user_password):
                user = user.pop()
                user.pop('password', None)
                return json_response(user)
        except:
            pass
        return json_response({})


class RefuellerAuthAPI(web.View):
    """
    Апи для проверки активации заправщика.
    """
    async def post(self):
        post_data = await self.request.json()
        user = {}
        sms_template = 'TankUp!%20Ваш%20проверочный%20код%0A{}'

        try:
            user = await fetch(self.request.app['pool'],
                               "select * from account where login='{}' "
                               "and user_type={} and is_active=True".format(post_data.get('login'), 2))
            if user:
                user = user.pop()
                random_code = randint(1000, 9999)
                user_code = await fetch(self.request.app['pool'], "update account set activation_code = '{}' "
                                                                  "where id={} RETURNING id".format(
                    random_code, user.get('id')))
                sms_result = await sms_main(post_data.get('login'), sms_template.format(random_code))
                return json_response(user)
        except Exception as e:
            pass

        return json_response(user)


class RefuellerCheckCodeAPI(web.View):
    """
    Апи для проверки СМС кода.
    """
    async def post(self):
        post_data = await self.request.json()
        user = {}
        try:
            user = await fetch(self.request.app['pool'],
                               "select * from account where login='{}' "
                               "and user_type={} and is_active=True and activation_code='{}'".format(
                                   post_data.get('login'), 2, post_data.get('code')))
            if user:
                user = user.pop()
                return json_response(user)
        except Exception as e:
            pass

        return json_response(user)
