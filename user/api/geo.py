import functools
import json
from collections import OrderedDict

from aiohttp import web
from aiohttp.web_response import json_response, Response

from db.db import fetch
from decorators import check_body_exist
from settings import SQL
from user.api.exception import YandexGeoError
from utils.api import ApiReturnAnswer
from utils.geo import call_yandex_geo


def custom_json_return(lon: float, lat: float, cache: bool = False) -> Response:
    return json_response({
        'lon': lon,
        'lat': lat,
        'cache': cache
    })


class GetAddressFromCoordinatesAPI(web.View):
    """
    Апи для получения адреса по координатам.
    """

    @check_body_exist
    async def post(self):
        try:
            post_data = await self.request.json()
            lon = post_data.get('lon', 0)
            lat = post_data.get('lat', 0)

            # cache_address = await fetch(self.request.app['pool'], SQL.get('user').get('cache')[0].format(lon, lat))
            #
            # if not cache_address:
            response = await call_yandex_geo('{},{}'.format(lon, lat))
            if not response:
                raise YandexGeoError

            address = '{}'.format(response.get('response').get('GeoObjectCollection').get(
                'featureMember')[0].get('GeoObject').get('name')
            )

                # address = '{}, {}'.format(
                #     response.get('response').get('GeoObjectCollection').get(
                #         'featureMember')[0].get('GeoObject').get('description'),
                #     response.get('response').get('GeoObjectCollection').get(
                #         'featureMember')[0].get('GeoObject').get('name')
                # )

            #     cache = await fetch(self.request.app['pool'], SQL.get('user').get('cache')[2].format(lon, lat, address))
            #     if not cache:
            #         return ApiReturnAnswer.bad('Ошибка при создании записи к кэш (GetAddressFromCoordinatesAPI).')
            #
            # else:
            #     resp = cache_address.pop()
            #     resp['cache'] = True
            #     return json_response(resp)

            return json_response({'address': address})
        except YandexGeoError as e:
            return ApiReturnAnswer.bad('Ошибка в запросе, при преобразовании координат в адрес.')


class GetCoordinatesFromAddressAPI(web.View):
    """
    Апи для получения координат по адресу.
    """

    @check_body_exist
    async def post(self):
        try:
            post_data = await self.request.json()
            address = post_data.get('address', '')

            # cache_coord = await fetch(self.request.app['pool'], SQL.get('user').get('cache')[1].format(address))
            #
            # if not cache_coord:
            response = await call_yandex_geo('{}'.format(post_data.get('address', '')), post_data.get('ll', ''))
            if not response:
                raise YandexGeoError
            coord = response.get('response').get('GeoObjectCollection').get(
                'featureMember')[0].get('GeoObject').get('Point').get('pos')

            if coord:
                coord = coord.split(' ')
            else:
                raise YandexGeoError

            cache = await fetch(self.request.app['pool'], SQL.get('user').get('cache')[2].format(
                coord[0], coord[1], address))
            if not cache:
                return ApiReturnAnswer.bad('Ошибка при создании записи к кэш (GetCoordinatesFromAddressAPI).')

            return custom_json_return(coord[0], coord[1])
            # else:
            #     cache_coord = cache_coord.pop()
            #
            #     return custom_json_return(cache_coord.get('lon', '-'), cache_coord.get('lat', '-'), True)

        except YandexGeoError as e:
            return ApiReturnAnswer.bad('Ошибка в запросе, при преобразовании адреса в координаты.')

