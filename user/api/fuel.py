from aiohttp import web
from aiohttp.web_response import json_response

from db.db import fetch
from settings import SQL
from utils.api import ApiReturnAnswer


class FuelAPI(web.View):
    """
    Апи для получения типа бензина и его стоимости.
    """
    async def get(self):
        try:
            fuel_type = await fetch(self.request.app['pool'], SQL.get('user').get('fuel')[0])
            if not fuel_type:
                return json_response(list())
            return json_response(fuel_type)
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка в запросе, при получении типов и цены бензина.')

