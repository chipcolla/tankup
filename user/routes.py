import collections

from user.api.fuel import FuelAPI
from user.api.geo import GetAddressFromCoordinatesAPI, GetCoordinatesFromAddressAPI
from user.api.order import CreateOrderAPI, GetActiveOrderAPI

dispatcher = collections.namedtuple('Dispatcher', ['method', 'path', 'handler', 'name'])


user_routes = [
    # API
    dispatcher('GET', '/api/v2/user/fuel/', FuelAPI, 'api_user_fuel'),
    dispatcher('POST', '/api/v2/user/order/create/', CreateOrderAPI, 'api_user_create_order'),
    dispatcher('GET', '/api/v2/user/order/active/', GetActiveOrderAPI, 'api_user_active_order'),
    dispatcher('POST', '/api/v2/user/coord/address/', GetAddressFromCoordinatesAPI, 'api_user_address_coord'),
    dispatcher('POST', '/api/v2/user/address/coord/', GetCoordinatesFromAddressAPI, 'api_user_coord_address'),
]
