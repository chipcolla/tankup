import jwt
from aiohttp.web_response import json_response

from settings import GLOBAL_SECRET


async def auth_middleware(app, handler):
    async def middleware(request):
        # f = await handler(request)
        token_verify = getattr(handler, 'TOKEN_VERIFY', True)
        if all([request.path != '/', bool(request.path.find('api/v2/user') == -1), token_verify]):
            try:
                jwt_token = request.headers.get('Authorization', None)
                if not jwt_token:
                    raise jwt.DecodeError

                secret_key = GLOBAL_SECRET.get('manager')
                if request.path.find('api/v2/refueller') != -1:
                    secret_key = GLOBAL_SECRET.get('refueller')

                payload = jwt.decode(jwt_token, secret_key, 'HS256')
            except (jwt.DecodeError, jwt.ExpiredSignatureError):
                return json_response({'message': 'Token is invalid'}, status=400)
            else:
                request.User = {
                    'id': payload.get('id'),
                    'login': payload.get('login'),
                    'user_type': payload.get('user_type'),
                }
        return await handler(request)
    return middleware
