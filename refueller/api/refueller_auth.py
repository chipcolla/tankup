from random import randint

from aiohttp import web
from aiohttp.web_response import json_response

from db.db import fetch
from decorators import check_body_exist
from settings import SMS_MESSAGES_TEMPLATE, SQL
from utils.api import ApiReturnAnswer
from utils.jwt_helper import build_token
from utils.sms import sms_main


class RefuellerAuthAPI(web.View):
    """
    Апи для проверки активации заправщика.
    """
    TOKEN_VERIFY = False

    @check_body_exist
    async def post(self):
        try:
            post_data = await self.request.json()
            sms_template = SMS_MESSAGES_TEMPLATE.get('check-sms')

            user = await fetch(self.request.app['pool'], SQL.get('refueller').get('refueller_auth')[0].format(
                post_data.get('login', 'anonymous')))
            if user:
                user = user.pop()
                # Заглушка для тестового номера (71111111111).
                if post_data.get('login') != '71111111111':
                    random_code = randint(1000, 9999)
                    user_code = await fetch(self.request.app['pool'], SQL.get('refueller').get(
                        'refueller_auth')[1].format(random_code, user.get('id')))
                    sms_answer = await sms_main(post_data.get('login'), sms_template.format(random_code))
                    if not sms_answer:
                        return ApiReturnAnswer.bad('Ошибка при отправке СМС сообщения.')
                return json_response(dict())
            return ApiReturnAnswer.bad('Заправщик: {}, не найден.'.format(post_data.get('login')))
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка при проверке заправщика.')


class RefuellerCheckCodeAPI(web.View):
    """
    Апи для проверки СМС кода.
    """
    TOKEN_VERIFY = False

    @check_body_exist
    async def post(self):
        try:
            post_data = await self.request.json()

            user = await fetch(self.request.app['pool'], SQL.get('refueller').get('refueller_auth')[2].format(
                post_data.get('login'), 2, post_data.get('code')))
            if user:
                user = user.pop()
                mp_id = await fetch(self.request.app['pool'], SQL.get('refueller').get('refueller_auth')[3].format(
                    post_data.get('user_id', ''), user.get('id')))
                if not mp_id:
                    return ApiReturnAnswer.bad('Ошибка при установке mp_id дя пушей.')
                return json_response({'token': build_token(user)})
            return ApiReturnAnswer.bad('Проверка СМС кода: {}, для заправщика: {} - провалена.'.format(
                post_data.get('code'), post_data.get('login')))
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка при проверке СМС кода заправщика.')
