from json import JSONDecodeError

from aiohttp import web
from aiohttp.web_response import json_response

from db.db import fetch
from decorators import check_body_exist
from settings import SMS_MESSAGES_TEMPLATE, SQL
from utils.api import ApiReturnAnswer
from utils.message import combined_message


class OrderCompleteCodeAPI(web.View):
    """
    Апи для завершения заказа.
    """

    @check_body_exist
    async def post(self):
        try:
            post_data = await self.request.json()
            order = await fetch(self.request.app['pool'], SQL.get('refueller').get('order')[0].format(
                post_data.get('order_id', 0)))
            if not order:
                return ApiReturnAnswer.bad('Не удается завершить заказ №{}.'.format(post_data.get('order_id', '')))

            # Получим заправщика.
            driver = await fetch(self.request.app['pool'], SQL.get('refueller').get('order')[1].format(
                order[0].get('driver', 0)
            ))
            if not driver:
                return ApiReturnAnswer.bad('Не получилось получить водителя: {}.'.format(post_data.get('driver_id')))

            # Для клиента.
            await combined_message(order[0].get('mp_id'), order[0].get('phone_number'),
                                   message=SMS_MESSAGES_TEMPLATE.get('order-complete-user'))

            # Для заправщика.
            await combined_message(driver[0].get('mp_id'), driver[0].get('login'),
                                   message=SMS_MESSAGES_TEMPLATE.get('order-complete-driver'))

            return json_response(dict())
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка в запросе, при завершении заказа.')


class AllOrdersForDriverAPI(web.View):
    """
    Апи получения всёх заказов по водителю.
    """
    async def get(self):
        try:
            user = self.request.User

            active_order = await fetch(self.request.app['pool'], SQL.get('refueller').get('order')[2].format(
                user.get('id')))
            if not active_order:
                return json_response(list())
            else:
                answer_list = []
                for order in active_order:
                    answer = {
                        'id': order.get('id'),
                        'liters': order.get('liters'),
                        'carNo': order.get('car_no'),
                        # Чем оплачено? ApplePay или картой!
                        'paymentType': order.get('payment_type'),
                        'orderStatus': order.get('order_status'),
                        'carModel': order.get('car_model'),
                        'name': order.get('o_name'),
                        'phoneNumber': order.get('phone_number')
                    }

                    driver_lat = order.get('driver_lat')
                    driver_lon = order.get('driver_lon')

                    if driver_lat and driver_lon:
                        answer['driver'] = {
                            'location': {
                                'lat': driver_lat,
                                'lon': driver_lon
                            }
                        }
                    else:
                        answer['driver'] = {
                            'location': {}
                        }

                    answer['fuel'] = {
                        'id': order.get('ft_id'),
                        'name': order.get('ft_name'),
                        'price': order.get('ft_price')
                    }

                    answer['location'] = {
                        'lat': order.get('lat'),
                        'lon': order.get('lon')
                    }

                    answer_list.append(answer)

                return json_response(answer_list)
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка в запросе, при получении заказов заправщика.')


class SetCoordForDriverAPI(web.View):
    """
    Апи установки широты/долготы в точке текущего нахождения водителя.
    """

    @check_body_exist
    async def post(self):
        try:
            post_data = await self.request.json()
            user = self.request.User
            if post_data.get('driver_id'):
                user['id'] = post_data.get('driver_id')

            coord = await fetch(self.request.app['pool'], SQL.get('refueller').get('order')[3].format(
                post_data.get('lat'), post_data.get('lon'), user.get('id', 0)))
            if not coord:
                return ApiReturnAnswer.bad('Ошибка при установки координат для заправщика: {}'.format(
                    user.get('id', '')))
            return json_response(dict())
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка в запросе, при установке координат.')


class GetCoordForDriverAPI(web.View):
    """
    Апи получения широты/долготы.
    """
    async def get(self):
        try:
            user = self.request.User

            driver_id = self.request.rel_url.query.get('driver_id')
            if driver_id:
                user['id'] = driver_id

            coord = await fetch(
                self.request.app['pool'], SQL.get('refueller').get('order')[4].format(user.get('id', 0)))
            if not coord:
                return ApiReturnAnswer.bad(
                    'Ошибка при получении координат для заправщика: {}'.format(user.get('id', '')))
            coord = coord.pop()
            return json_response(coord)
        except Exception as e:
            return ApiReturnAnswer.bad('Ошибка в запросе, при получении координат.')


