import collections

from refueller.api.order import OrderCompleteCodeAPI, AllOrdersForDriverAPI, SetCoordForDriverAPI, GetCoordForDriverAPI
from refueller.api.refueller_auth import RefuellerAuthAPI, RefuellerCheckCodeAPI
from user.api.fuel import FuelAPI

dispatcher = collections.namedtuple('Dispatcher', ['method', 'path', 'handler', 'name'])


refueller_routes = [
    # API
    dispatcher('GET', '/api/v2/refueller/fuel/', FuelAPI, 'api_refueller_fuel'),
    dispatcher('POST', '/api/v2/refueller/auth/', RefuellerAuthAPI, 'api_auth_refueller'),
    dispatcher('POST', '/api/v2/refueller/auth/check-code/', RefuellerCheckCodeAPI, 'api_auth_refueller_check_code'),
    dispatcher('POST', '/api/v2/refueller/order/complete/', OrderCompleteCodeAPI, 'api_order_complete_refueller'),
    dispatcher('GET', '/api/v2/refueller/order/all/', AllOrdersForDriverAPI, 'api_order_all_refueller'),
    dispatcher('POST', '/api/v2/refueller/coord/set/', SetCoordForDriverAPI, 'api_coord_set_refueller'),
    dispatcher('GET', '/api/v2/refueller/coord/get/', GetCoordForDriverAPI, 'api_coord_get_refueller'),
]
