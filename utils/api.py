from aiohttp.web_response import json_response, Response


def build_return_answer(self, message, status_code=200):
    return json_response({'message': message}, status=status_code)


class ApiReturnAnswer:
    @staticmethod
    def make_message(message: str, status_code: int = 200) -> Response:
        return json_response({'message': message}, status=status_code)

    @staticmethod
    def good(message: str) -> Response:
        return ApiReturnAnswer.make_message(message)

    @staticmethod
    def bad(message: str) -> Response:
        return ApiReturnAnswer.make_message(message, status_code=400)
