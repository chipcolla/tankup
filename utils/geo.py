import json
from typing import Union, Dict

import aiohttp

from settings import YANDEX_GEO_TOKEN


async def call_yandex_geo(geocode: str, ll: str = None) -> Union[bool, Dict]:
    async with aiohttp.ClientSession() as session:

        for token in YANDEX_GEO_TOKEN:
            async with session.get('https://geocode-maps.yandex.ru/1.x/?format=json&apikey={}&geocode={}{}'.format(
                    token,
                    geocode,
                    '&ll={}'.format(ll) if ll else ''
            )) as response:
                try:
                    if response.status == 403:
                        raise TypeError
                except (TypeError, OverflowError):
                    continue
                return await response.json()
        return False
