from asyncio import get_event_loop
from functools import partial
from typing import Union, Dict

from aiohttp.web_response import Response
from pyfcm import FCMNotification
from pyfcm.errors import FCMError

from settings import pool, push_service
from utils.api import ApiReturnAnswer
from utils.sms import sms_main

loop = get_event_loop()


async def sms_message(phone: str, message: str, error_message: str = None) -> Union[bool, Response]:
    sms_answer = await sms_main(phone, message)
    if not sms_answer:
        return ApiReturnAnswer.bad(error_message if error_message else 'Ошибка сервиса отправки смс - уведомлений.')
    return True


async def push_message(registration_id: str, message_title: str, message_body: str, error_message: str = None) -> \
        Union[bool, Response]:
    try:
        # push_notification = await loop.run_in_executor(pool, lambda: push_service.notify_single_device(
        #     registration_id=registration_id,
        #     message_title=message_title,
        #     message_body=message_body
        # ))
        push_notification = await loop.run_in_executor(pool, partial(push_service.notify_single_device,
                                                                     registration_id=registration_id,
                                                                     message_title=message_title,
                                                                     message_body=message_body
                                                                     )
                                                       )
        if not push_notification.get('success'):
            return False
    except FCMError:
        return ApiReturnAnswer.bad(error_message if error_message else 'Ошибка сервиса отправки пуш - уведомлений.')
    return True


async def combined_message(token: str, phone: str, title: str = 'Информация о заказе', message: str = None) -> \
        Union[FCMNotification, bool, Dict]:
    pm = None
    if token:
        pm = await push_message(token, title, message.replace('%20', ' ').replace('%0A', ' '))
    if not any([token, pm]):
        return await sms_message(phone
                                 .replace('(', '')
                                 .replace(')', '')
                                 .replace('-', '')
                                 .replace(' ', ''), message)
    return pm

