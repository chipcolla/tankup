"""
Функции для работы с СМС.
"""
# import asyncio
from typing import Dict, Union

import aiohttp

LOGIN = ''
PASSWORD = ''


async def check_balance():
    async with aiohttp.ClientSession() as session:
        async with session.get('https://smsc.ru/sys/balance.php?login={}&psw={}'.format(LOGIN, PASSWORD)) as response:
            response = await response.read()
            return float(response)


async def send_sms(phone: str, message: str, cost: bool = False) -> Dict:
    async with aiohttp.ClientSession() as session:
        async with session.get('https://smsc.ru/sys/send.php?login={}&psw={}&phones={}&mes={}&fmt=3{}'.format(
                LOGIN,
                PASSWORD,
                phone,
                message,
                '&cost=1' if cost else ''
        )) as response:
            # response = await response.read()
            response = await response.json()
            return response


async def sms_main(phone: str, message: str) -> Union[Dict, None]:
    balance = await check_balance()
    sms_send_cost = await send_sms(phone, message, True)
    if balance > float(sms_send_cost.get('cost')):
        send = await send_sms(phone, message)
        return send

