"""
Payload.
Построение токена для jwt.
"""

from typing import Dict

import jwt

from settings import GLOBAL_SECRET


def build_token(user: Dict) -> str:
    token = jwt.encode({
        'id': user.get('id'),
        'login': user.get('login'),
        'user_type': user.get('user_type'),
        'verify_exp': False
    }, GLOBAL_SECRET.get('manager' if user.get('user_type') == 1 else 'refueller'), algorithm='HS256')
    return token.decode("utf-8")

