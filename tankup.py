from asyncio import get_event_loop

import aiohttp_cors
import aiohttp_jinja2
import asyncpg
import jinja2
from aiohttp import web, WSCloseCode

from middleware.token import auth_middleware
from routes import routes
from settings import DB_SETTINGS


class TankUpView(web.View):
    async def get(self):
        return aiohttp_jinja2.render_template('index.html', self.request, {
        })


async def on_shutdown(app):
    for ws in set(app['sockets']):
        await ws.close(code=WSCloseCode.GOING_AWAY, message='Server shutdown')


async def tankup_web_app():
    app = web.Application(middlewares=[auth_middleware])
    app['pool'] = await asyncpg.create_pool(**DB_SETTINGS)
    app['sockets'] = []
    for route in routes:
        app.router.add_route(
            method=route.method,
            path=route.path,
            handler=route.handler,
            name=route.name
        )
    app.router.add_route(method='GET', path='/', handler=TankUpView, name='tankup_home')

    aiohttp_jinja2.setup(app, loader=jinja2.FileSystemLoader('/home/web/tankup/templates/'))

    cors = aiohttp_cors.setup(app, defaults={
        '*': aiohttp_cors.ResourceOptions(
            allow_credentials=True,
            expose_headers='*',
            allow_headers='*',
            allow_methods='*'
        )
    })
    for route in list(app.router.routes()):
        cors.add(route)

    return app

# Заводим в песочнице
# loop = get_event_loop()
# _app = loop.run_until_complete(tankup_web_app())
# web.run_app(_app, host='localhost')
