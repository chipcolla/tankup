import collections

from manager.routes import manager_routes
from refueller.routes import refueller_routes
from user.routes import user_routes

dispatcher = collections.namedtuple('Dispatcher', ['method', 'path', 'handler', 'name'])

routes = user_routes
routes += manager_routes
routes += refueller_routes

