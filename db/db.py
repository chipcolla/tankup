"""
Запросы к БД
"""
from typing import Any, List, Dict

from asyncpg.pool import Pool


async def fetch(pool: Pool, query: str, *args: Any) -> List[Dict]:
    async with pool.acquire() as connection:
        async with connection.transaction():
            result = await connection.fetch(query)
            return [dict(row) for row in result]
